
unsigned long pipeFillingTime(int lengthInCentimeters) {
    int internalLength = lengthInCentimeters / 10;
    double result = 500 - internalLength * 15 + internalLength * internalLength * 7.0;
    return (unsigned long) result;
}
